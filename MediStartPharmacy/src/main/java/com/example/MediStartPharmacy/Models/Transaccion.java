/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStartPharmacy.Models;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * farmaciaVirtual MediStartPharmacy
 *
 * @author Carlos Alberto Sánchez Medina
 */
// esta entidad siempre se deja pegada al nombre de la clase
// esto se necesita para indicar que la clase Transaccion es una entidad  ...*** con la tabla "categorias" // 

public class Transaccion {
// autowire permite inyectar codigo Sql en java

    @Autowired
    transient  JdbcTemplate jdbcTemplate ;  // JdbcTemplate es una clase que permite ejecutar   ***** 

    private int IdTransaccion;
    private Factura Fk_IdFactura;   // foreign key ???*****
    private Producto Fk_IdProducto;   // foreign key ???*****
    private int CantidadSalida;
    private float ValorUnitarioVnt;
    

    public Transaccion() {
    }

    public Transaccion(int IdTransaccion, int CantidadSalida, float ValorUnitarioVnt) {
        this.IdTransaccion = IdTransaccion;
        this.CantidadSalida = CantidadSalida;
        this.ValorUnitarioVnt = ValorUnitarioVnt;
    }

    public int getIdTransaccion() {
        return IdTransaccion;
    }

    public void setIdTransaccion(int IdTransaccion) {
        this.IdTransaccion = IdTransaccion;
    }

    public Factura getFk_IdFactura() {
        return Fk_IdFactura;
    }

    public void setFk_IdFactura(Factura Fk_IdFactura) {
        this.Fk_IdFactura = Fk_IdFactura;
    }

    public Producto getFk_IdProducto() {
        return Fk_IdProducto;
    }

    public void setFk_IdProducto(Producto Fk_IdProducto) {
        this.Fk_IdProducto = Fk_IdProducto;
    }

    public int getCantidadSalida() {
        return CantidadSalida;
    }

    public void setCantidadSalida(int CantidadSalida) {
        this.CantidadSalida = CantidadSalida;
    }

    public float getValorUnitarioVnt() {
        return ValorUnitarioVnt;
    }

    public void setValorUnitarioVnt(float ValorUnitarioVnt) {
        this.ValorUnitarioVnt = ValorUnitarioVnt;
    }

    @Override
    public String toString() {
        return "Transaccion{" + "IdTransaccion=" + IdTransaccion + ", Fk_IdFactura=" + Fk_IdFactura + ", Fk_IdProducto=" + Fk_IdProducto + ", CantidadSalida=" + CantidadSalida + ", ValorUnitarioVnt=" + ValorUnitarioVnt + '}';
    }


    // CRUD   
    //CRUD -R  true
    // Consultar SI EXISTE => return  true
    public boolean consultarTransaccion() throws ClassNotFoundException, SQLException {
        String sql = "SELECT IdTransaccion,Fk_IdFactura,Fk_IdProducto,CantidadSalida,ValorUnitarioVnt FROM transacciones WHERE Pk_idtransaccion = ?";
        List<Transaccion> transacciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Transaccion(
                        rs.getInt("IdTransaccion"), // => 123
                        rs.getInt("CantidadSalida"),
                        rs.getFloat("ValorUnitarioVnt")
                ), new Object[]{this.getIdTransaccion()});
        if (transacciones != null && transacciones.isEmpty() ) {
            this.setIdTransaccion(transacciones.get(0).getIdTransaccion());
            this.setCantidadSalida(transacciones.get(0).getCantidadSalida());
            this.setValorUnitarioVnt(transacciones.get(0).getValorUnitarioVnt());
            return true;    // existe el registro
        } else {
            return false;    // el registro no existe
        }
    }

    //CRUD -R all
    
    
    public List<Transaccion> consultarTodoTransaccion(int IdTransaccion) throws ClassNotFoundException, SQLException {
        String sql = "SELECT IdTransaccion,Fk_IdFactura,Fk_IdProducto,CantidadSalida,ValorUnitarioVnt FROM transacciones WHERE IdTransaccion = ?";
        List<Transaccion> transacciones = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Transaccion(
                        rs.getInt("IdTransaccio"), // => 123
                        rs.getInt("CantidadSalida"),
                        rs.getFloat("ValorUnitarioVnt")
                        ), new Object[]{this.getIdTransaccion()});

        return transacciones;     // devuelve la lista con los datos  
    }

// crud  update   //  Gonzalo  aqui falta lineas de codigo
    public String actualizarTransaccion() {
        String sql = "UPDATE transacciones SET Fk_IdFactura,Fk_IdProducto,CantidadSalida,ValorUnitarioVnt FROM transacciones WHERE IdTransaccion = ?";
        *************    CODIGO DE ESTEFANIA  ************
                Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.getNombre());
        ps.setString(2, this.getCorreo());
        ps.setInt(3, this.getEdad());
        ps.setInt(4, this.getId());
        ps.executeUpdate();
        ps.close();
                
                
                
                *******   FIN DE CODIGO DE ESTAFANIA *****************
        
                
    }

//  borra y returna  true
    public boolean borrarTransaccion() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM transacciones WHERE IdTransaccion = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection(); //  aqui quede ****
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getIdTransaccion());
        ps.execute();
        ps.close();
        return true;

    }
    public int guardarTransaccion() throws ClassNotFoundException, SQLException{
        int last_inserted_id = -1;
        String  sql = "INSERT INTO transacciones(IdTransaccion,Fk_IdFactura,Fk_IdProducto,CantidadSalida,ValorUnitarioVnt) VALUES(?,?,?,?,?)";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.IdTransaccion);
        ps.setInt(2,this.Fk_IdFactura);
        ps.setInt(3, Fk_IdProducto);
        ps.setInt(4, this.CantidadSalida);
        ps.setFloat(4, this.ValorUnitarioVnt);
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()){
            last_inserted_id = rs.getInt(1);
        }
        ps.close();
        return last_inserted_id;
    }
}