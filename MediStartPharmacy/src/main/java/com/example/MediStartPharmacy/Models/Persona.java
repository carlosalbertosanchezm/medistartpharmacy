/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 * farmaciaVirtual MediStartPharmacy
 * Esta clase permite hacer el manejo integral de las Personas de la Farmacia.
 * En este documento en espanol se trato de evitar ls tildes y las enes   hahaha
 *
 * Criterios de variables
 *   solo letras a excepción de las llaves foraneas
 * a) Base de datos
 *  nombres de las tablas en plural y minusculas
 *  nombres de los atributos de las tablas PascalCase, dos palabras completas, si se requiere, para las palabras adicionales
 *  solo las tres primeras consonantes, ejemplo atributo   valor Unitario de Compra =  ValorUnitarioCmp
 *  en todas las variables se indica a que tabla pertenecen con la ultima palabra.
 *  las llaves foraneas se denominan así   Fk_NameAtributte    donde NameAtributte es el nombre de la primary key de la tabla referenciada.
 * b)Clases
 *   todas en singular PascalCase  ejemplo clase Persona
 * c) variables en Java
 *  todas empiezan con Minusculas,  camelCase   ejemplo  valorUnitarioCmp
 *
 * @author Carlos Alberto Sánchez Medina
 *
 * Que encontrará en este archivo
 * 1. Package and imports
 * 2. @ arrobas
 * 3. Atributtes
 * 4. Builders
 * 5. Getters and Setters
 * 6. ToString
 * 7. CRUD
 */
/*
  1. Package and imports
 */
package com.example.MediStartPharmacy.Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;




/*
* 2. @ arrobas 
 */
// esta entidad siempre se deja pegada al nombre de la clase
// esto se necesita para indicar que la clase Persona es una entidad con la tabla "listPersonas" de la base de datos// 
@Component("personas")
public class Persona {

    @Autowired // autowire permite inyectar codigo Sql en java
    transient JdbcTemplate jdbcTemplate;  // JdbcTemplate es una clase que permite ejecutar   las consulta sql
    // Puede que no siempre se ejecute la consulta.  
    // este "atributo" no debe hacer parte del constructor ni de los gettres ni stters ni del tostring

    /* 
* 3. Atributtes
     */
    private int idPersona;    // este idPersona   es el nombre del atributo en Java Netbeans
    private String rolPersona;
    private String nombrePersona;
    private String apellidoPersona;
    private String usuarioPersona;
    private String clavePersona;
    private String tipoIdentificacion;
    private String numeroIdentificacion;
    private String direccionPersona;
    private String ciudadPersona;
    private String emailPersona;
    private String telefonoPersona;
    private String fechaLogging; // que tipo de dato para fechas en vez de String ?***
// si hay una foreign key  entonces
    // private Estudiante id-estudiante  // es la forma de colocar llave foranea  de la clase estudiante

    /*
    * 4. Builders
     */
    // empty builder
    public Persona() {
    }
// complete builder    
// Constructor - NO VA LA LLAVE FORANEA, ni va el JdbcTemplate

    public Persona(int idPersona, String rolPersona, String nombrePersona, String apellidoPersona, String usuarioPersona, String clavePersona, String tipoIdentificacion, String numeroIdentificacion, String direccionPersona, String ciudadPersona, String emailPersona, String telefonoPersona, String fechaLogging) {
        this.idPersona = idPersona;
        this.rolPersona = rolPersona;
        this.nombrePersona = nombrePersona;
        this.apellidoPersona = apellidoPersona;
        this.usuarioPersona = usuarioPersona;
        this.clavePersona = clavePersona;
        this.tipoIdentificacion = tipoIdentificacion;
        this.numeroIdentificacion = numeroIdentificacion;
        this.direccionPersona = direccionPersona;
        this.ciudadPersona = ciudadPersona;
        this.emailPersona = emailPersona;
        this.telefonoPersona = telefonoPersona;
        this.fechaLogging = fechaLogging;
    }

    /*    
    * 5. Getters and Setters
        nameAtributte= nombre del atributo ->   GETnameAtributte = mostrar o consultar el atributo y  SETnameAtributte = modificar el Atributte     
        aqui no va el JdbcTemplate
        aqui se deben incluir las llaves foraneas en caso de existir
     */
    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getRolPersona() {
        return rolPersona;
    }

    public void setRolPersona(String rolPersona) {
        this.rolPersona = rolPersona;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPersona() {
        return apellidoPersona;
    }

    public void setApellidoPersona(String apellidoPersona) {
        this.apellidoPersona = apellidoPersona;
    }

    public String getUsuarioPersona() {
        return usuarioPersona;
    }

    public void setUsuarioPersona(String usuarioPersona) {
        this.usuarioPersona = usuarioPersona;
    }

    public String getClavePersona() {
        return clavePersona;
    }

    public void setClavePersona(String clavePersona) {
        this.clavePersona = clavePersona;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getDireccionPersona() {
        return direccionPersona;
    }

    public void setDireccionPersona(String direccionPersona) {
        this.direccionPersona = direccionPersona;
    }

    public String getCiudadPersona() {
        return ciudadPersona;
    }

    public void setCiudadPersona(String ciudadPersona) {
        this.ciudadPersona = ciudadPersona;
    }

    public String getEmailPersona() {
        return emailPersona;
    }

    public void setEmailPersona(String emailPersona) {
        this.emailPersona = emailPersona;
    }

    public String getTelefonoPersona() {
        return telefonoPersona;
    }

    public void setTelefonoPersona(String telefonoPersona) {
        this.telefonoPersona = telefonoPersona;
    }

    public String getFechaLogging() {
        return fechaLogging;
    }

    public void setFechaLogging(String fechaLogging) {
        this.fechaLogging = fechaLogging;
    }


    /*
    * 6. ToString
    aqui no va el JdbcTemplate
     */
    @Override
    public String toString() {
        return "Persona{" + "idPersona=" + idPersona + ", rolPersona=" + rolPersona + ", nombrePersona=" + nombrePersona + ", apellidoPersona=" + apellidoPersona + ", usuarioPersona=" + usuarioPersona + ", clavePersona=" + clavePersona + ", tipoIdentificacion=" + tipoIdentificacion + ", numeroIdentificacion=" + numeroIdentificacion + ", direccionPersona=" + direccionPersona + ", ciudadPersona=" + ciudadPersona + ", emailPersona=" + emailPersona + ", telefonoPersona=" + telefonoPersona + ", fechaLogging=" + fechaLogging + '}';
    }

    /*
    * 7. CRUD
     */
// CRUD
// Create = Guardar nuevo registro 
    //GUARDAR
    public int guardarPersona() throws ClassNotFoundException, SQLException {
        int last_inserted_id = -1;
        String sql = "INSERT INTO personas (RolPersona,NombrePersona, ApellidoPersona, UsuarioPersona, ClavePersona, TipoIdentificacion, NumeroIdentificacion, DireccionPersona,CiudadPersona, EmailPersona,TelefonoPersona, FechaLogging) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(1, this.rolPersona);
        ps.setString(2, this.nombrePersona);
        ps.setString(3, this.apellidoPersona);
        ps.setString(4, this.usuarioPersona);
        ps.setString(5, this.clavePersona);
        ps.setString(6, this.tipoIdentificacion);
        ps.setString(7, this.numeroIdentificacion);
        ps.setString(8, this.direccionPersona);
        ps.setString(9, this.ciudadPersona);
        ps.setString(10, this.emailPersona);
        ps.setString(11, this.telefonoPersona);
        ps.setString(12, this.fechaLogging);
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next()) {
            last_inserted_id = rs.getInt(1);
        }
        ps.close();
        return last_inserted_id;
    }

    // Consultar persona devuelve true si existe el registro
    public boolean consultarPersona() throws ClassNotFoundException, SQLException {
        String sql = "SELECT IdPersona,RolPersona,NombrePersona, ApellidoPersona, UsuarioPersona, ClavePersona, "
                + "TipoIdentificacion, NumeroIdentificacion, DireccionPersona,CiudadPersona, EmailPersona,TelefonoPersona, "
                + "FechaLogging FROM personas WHERE IdPersona = ?";
        List<Persona> listaPersonas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Persona(
                        rs.getInt("IdPersona"), // => 123
                        rs.getString("RolPersona"), // => 
                        rs.getString("NombrePersona"), // => 
                        rs.getString("ApellidoPersona"), // => 
                        rs.getString("UsuarioPersona"), // => 
                        rs.getString("ClavePersona"), // => 
                        rs.getString("TipoIdentificacion"), // => 
                        rs.getString("NumeroIdentificacion"), // => 
                        rs.getString("DireccionPersona"), // => 
                        rs.getString("CiudadPersona"), // => 
                        rs.getString("EmailPersona"), // => 
                        rs.getString("TelefonoPersona"), // => 
                        rs.getString("FechaLogging"), // => 
                      //  rs.getFloat("saldo") si fuera un flotante
                ), new Object[]{this.getIdPersona()});
        if (listaPersonas != null && !listaPersonas.isEmpty()) {
            this.setIdPersona(listaPersonas.get(0).getIdPersona());
            this.setRolPersona(listaPersonas.get(0).getRolPersona());
            this.setNombrePersona(listaPersonas.get(0).getNombrePersona());
            this.setApellidoPersona(listaPersonas.get(0).getApellidoPersona());
            this.setUsuarioPersona(listaPersonas.get(0).getUsuarioPersona());
            this.setClavePersona(listaPersonas.get(0).getClavePersona());
            this.setTipoIdentificacion(listaPersonas.get(0).getTipoIdentificacion());
            this.setNumeroIdentificacion(listaPersonas.get(0).getNumeroIdentificacion());
            this.setDireccionPersona(listaPersonas.get(0).getDireccionPersona());
            this.setCiudadPersona(listaPersonas.get(0).getCiudadPersona());
            this.setEmailPersona(listaPersonas.get(0).getEmailPersona());
            this.setTelefonoPersona(listaPersonas.get(0).getTelefonoPersona());
            this.setFechaLogging(listaPersonas.get(0).getFechaLogging());
            return true;
        } else {
            return false;
        }
    }
    
    public List<Persona> consultarTodoPersona(int IdPersona) throws ClassNotFoundException, SQLException {
        String sql = "SELECT IdPersona, RolPersona,NombrePersona, ApellidoPersona, UsuarioPersona, ClavePersona,"
                + " TipoIdentificacion, NumeroIdentificacion, DireccionPersona,CiudadPersona,"
                + " EmailPersona,TelefonoPersona, FechaLogging) FROM personas WHERE IdPersona = ?";
        List<Persona> listaPersonas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Persona(
                        rs.getInt("IdPersona"), // => 123
                        rs.getString("RolPersona"),
                        rs.getString("NombrePersona"),
                        rs.getString("ApellidoPersona"),
                        rs.getString("UsuarioPersona"),
                        rs.getString("ClavePersona"),
                        rs.getString("TipoIdentificacion"),
                        rs.getString("NumeroIdentificacion"),
                        rs.getString("DireccionPersona"),
                        rs.getString("CiudadPersona"),
                        rs.getString("EmailPersona"),
                        rs.getString("TelefonoPersona"),
                        rs.getString("FechaLogging"),
                ), new Object[]{this.getIdPersona()});
        //
        return listaPersonas;

    }

    
    // aqui voy  
    // CRUD - Upadate = Actualizar 
    public String actualizarPersona() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE personas SET numero_cuenta = ?, cvv = ?, clave = ?, saldo = ? WHERE IdPersona = ?";
        return sql;
    }

    // CRUD - Delete = eliminar registro
    public boolean borrarPersona() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM personas WHERE IdPersona = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql); // PreparedStatement nos obliga a iniciar posiciones desde 1
        ps.setInt(1, this.getIdPersona());
        ps.execute();
        ps.close();

        return true;
    }
    
}
