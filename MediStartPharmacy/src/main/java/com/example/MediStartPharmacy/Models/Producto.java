
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStartPharmacy.Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author PC
 */
@Component("productos")
public class Producto {
   @Autowired
   transient JdbcTemplate jdbcTemplate;
   
   //Atributos
   private int IdProducto;
   private String NombreProducto;
   private String CategoriaProducto;
   private float ValorUnitarioPrd;
   private int CantidadInventario;

   //Constructor
    public Producto(int IdProducto, String NombreProducto, String CategoriaProducto, float ValorUnitarioPrd, int CantidadInventario) {
        this.IdProducto = IdProducto;
        this.NombreProducto = NombreProducto;
        this.CategoriaProducto = CategoriaProducto;
        this.ValorUnitarioPrd = ValorUnitarioPrd;
        this.CantidadInventario = CantidadInventario;
    }

    public Producto() {
    }
   
   //Metodos GET  &  SET

    public int getIdProducto() {
        return IdProducto;
    }

    public void setIdProducto(int IdProducto) {
        this.IdProducto = IdProducto;
    }

    public String getNombreProducto() {
        return NombreProducto;
    }

    public void setNombreProducto(String NombreProducto) {
        this.NombreProducto = NombreProducto;
    }

    public String getCategoriaProducto() {
        return CategoriaProducto;
    }

    public void setCategoriaProducto(String CategoriaProducto) {
        this.CategoriaProducto = CategoriaProducto;
    }

    public float getValorUnitarioPrd() {
        return ValorUnitarioPrd;
    }

    public void setValorUnitarioPrd(float ValorUnitarioPrd) {
        this.ValorUnitarioPrd = ValorUnitarioPrd;
    }

    public int getCantidadInventario() {
        return CantidadInventario;
    }

    public void setCantidadInventario(int CantidadInventario) {
        this.CantidadInventario = CantidadInventario;
    }

    @Override
    public String toString() {
        return "Producto{" + "IdProducto=" + IdProducto + ", NombreProducto=" + NombreProducto + ", CategoriaProducto=" + CategoriaProducto + ", ValorUnitarioPrd=" + ValorUnitarioPrd + ", CantidadInventario=" + CantidadInventario + '}';
    }
    
    //CRUD
    //GUARDAR    * aqui en guardarProducto hace falta aumentar todo el codigo con la nueva version de Estefania
    //****
    public String guardarProducto() throws ClassNotFoundException, SQLException{
        String  sql = "INSERT INTO productos(IdProducto, NombreProducto, CategoriaProducto, ValorUnitarioPrd, CantidadInventario) VALUES(?,?,?,?)";
        return sql;
    }
     //CONSULTAR *** falta consultarProducto    devuelve true hace falta *****
    
    ///
    //
    //
    ///
    //
    
    
    
    //CONSULTAR devuelve una lista 
    public List<Producto> consultarTodoProducto() throws ClassNotFoundException, SQLException{
        String  sql = "SELECT IdProducto, NombreProducto, CategoriaProducto, ValorUnitarioPrd, CantidadInventario FROM productos";
        List<Producto> prd = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Producto(
                        rs.getInt("IdProducto"), // => 123
                        rs.getString("NombreProducto"), // => 
                        rs.getString("CategoriaProducto"),
                       rs.getFloat("ValorUnitarioPrd"),
                       rs.getInt("CantidadInventario")));
        
        return prd; 
    }
    
    //ACTUALIZAR
    public String actualizarProducto() throws ClassNotFoundException, SQLException{
        String sql = "UPDATE productos SET NombreProducto = ?, CategoriaProducto = ?, ValorUnitarioPrd = ?, CantidadInventario = ? WHERE IdProducto = ?";
        return sql;
    }
    
    //ELIMINAR
    public boolean borrarProducto() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM productos WHERE IdProducto = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql); // PreparedStatement nos obliga a iniciar posiciones desde 1
        ps.setInt(1, this.IdProducto);
        ps.execute();
        ps.close();
        
        return true;
    }
}
