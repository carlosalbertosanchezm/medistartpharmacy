/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.MediStartPharmacy.Models;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Felipe G
 */
@Component("facturas")
public class Factura {

//PAQUETE QUE EJECUTA LAS CONSULTAS A SQL    
@Autowired
    transient JdbcTemplate jdbcTemplate; //TEMPLATE, PALABRA CLAVE EN JAVA PARA SERIALIZAR

//ATRIBUTOS
private int IdFactura; //LLAVE PRIMARIA - PRIMARY kEY (PK)
private String FacturaVenta;
private float ValorTotalFct;
private String FechaFactura;
private Persona Fk_IdPersona; //LLAVE FORANEA - FOREING KEY (FK)

    // METODO CONSTRUCTOR - NO VA PK NI FK
    public Factura(int IdFactura, String FacturaVenta, float ValorTotalFct, String FechaFactura) {  
        this.IdFactura = IdFactura;
        this.FacturaVenta = FacturaVenta;
        this.ValorTotalFct = ValorTotalFct;
        this.FechaFactura = FechaFactura;
    }

    // METODO CONSTRUCTOR VACIO
    public Factura() {
    }
    
    // METODO GETTER Y SETTER - NO VA SETTER DE FK

    public int getIdFactura() {
        return IdFactura;
    }

    public void setIdFactura(int IdFactura) {
        this.IdFactura = IdFactura;
    }

    public String getFacturaVenta() {
        return FacturaVenta;
    }

    public void setFacturaVenta(String FacturaVenta) {
        this.FacturaVenta = FacturaVenta;
    }

    public float getValorTotalFct() {
        return ValorTotalFct;
    }

    public void setValorTotalFct(float ValorTotalFct) {
        this.ValorTotalFct = ValorTotalFct;
    }

    public String getFechaFactura() {
        return FechaFactura;
    }

    public void setFechaFactura(String FechaFactura) {
        this.FechaFactura = FechaFactura;
    }
    
    
    // tostring

    @Override
    public String toString() {
        return "Factura{" + "IdFactura=" + IdFactura + ", FacturaVenta=" + FacturaVenta + ", ValorTotalFct=" + ValorTotalFct + ", FechaFactura=" + FechaFactura + ", Fk_IdPersona=" + Fk_IdPersona + '}';
    }
    
    
 
     //CRUD GUARDAR C
    public int guardarFactura() throws ClassNotFoundException, SQLException{
        int last_inserted_id = -1;
      //  String  sql = "INSERT INTO facturas(IdFactura,Fk_IdPersona, FacturaVenta, ValorTotalFct, FechaFactura) VALUES(?,?,?,?,?)";
        String  sql = "INSERT INTO facturas(IdFactura, FacturaVenta, ValorTotalFct, FechaFactura) VALUES(?,?,?,?)";  // no se que poner aquí ****
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
          //    ps.setString(1, this.Fk_IdPersona);   // no se que hacer aqui ********
        ps.setString(2, this.FacturaVenta);
        ps.setFloat(3,this.ValorTotalFct);
        ps.setString(4, this.FechaFactura);
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next()){
            last_inserted_id = rs.getInt(1);
        }
        ps.close();
        return last_inserted_id;
    }
    
    //CRUD CONSULTAR R (Read) Verdadero o falso = existe o no existe el registro
    public boolean consultarFactura() throws ClassNotFoundException, SQLException {
        String sql = "SELECT IdFactura, FacturaVenta, ValorTotal, FechaFactura FROM facturas WHERE Fk_IdPersona = ?";
        List<Factura> factura = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Factura(
                        rs.getInt("IdFactura"), // => 123
                        rs.getString("FacturaVenta"),
                        rs.getFloat("ValorTotalFct"),
                        rs.getString("FechaFactura"),
                   //     rs.getInt(getIdPersona("Fk_IdPersona")), // aqui no se que poner **** => 
                ), new Object[]{this.getIdFactura()});
        if (factura != null && !factura.isEmpty()) {
            this.setId(factura.get(0).getIdFactura());
            this.setFacturaVenta(factura.get(0).getFacturaVenta());
            this.setValorVenta(factura.get(0).getValorTotalFct());
            this.setFechaFactura(factura.get(0).getFechaFactura());
           
          
            return true; //EXISTE EL REGISTRO
        } else {
            return false; //NO EXISTE EL REGISTRO
        }
    }


    public List<Factura> consultarTodoFactura(int Id) throws ClassNotFoundException, SQLException {
        String sql = "SELECT id,Fk_Idpersonas, FacturaVenta, ValorTotal, FechaFactura FROM facturas WHERE IdFacturas = ?";
        List<Factura> facturas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Factura(
                        rs.getInt("IdFactura"), // => 123
                        rs.getInt("FacturaVenta"),
                        rs.getInt("ValorTotalFct"),
                        rs.getFloat("FechaFactura"),
                        rs.getInt("Fk_IdPersona"), // =>
                        ), new Object[]{this.getIdFactura()});

        return facturas;     //DEVUELVE LA LISTA CON TODOS LOS DATOS  
    }

    // CRUD ACTUALIZAR U (update)
    public String actualizarFactura() {
        String sql = "UPDATE transacciones SET Fk_Idpersonas, FacturaVenta, ValorTotal, FechaFactura FROM facturas WHERE IdFactura = ?";
        return sql;

    }

//  CRUD BORRAR O ELIMINAR D (Delete)
       public boolean borrarFactura() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM facturas WHERE IdFactura = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql); // PreparedStatement nos obliga a iniciar posiciones desde 1
        ps.setInt(1, this.IdFactura);
        ps.execute();
        ps.close();
        
        return true;
    }
    
    
}


