package com.example.MediStartPharmacy;

import com.example.MediStartPharmacy.Models.Factura;
import com.example.MediStartPharmacy.Models.Persona;
import com.example.MediStartPharmacy.Models.Producto;
import com.example.MediStartPharmacy.Models.Transaccion;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;






public class MediStartPharmacyApplication {

    @Autowired(required=true) // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Producto claseProducto; // Instancias - Objeto 
    @Autowired(required=true)
    Persona clasePersona; // Instancia
    @Autowired(required=true) // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Factura claseFactura; // Instancias - Objeto 
  
    public static void main(String[] args) {
        SpringApplication.run(MediStartPharmacyApplication.class, args);
    }
    // EJEMPLO
    @GetMapping("/hello")  // hello es el nombre que apareceen el buscados
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "20") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);
    }

    
    
    @GetMapping("/persona")
    public String consultarPersonaPorCedula(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        clasePersona.setIdPersona(cedula);
        if (clasePersona.consultarPersona()) { // True
            String respuestaPersonaPorCedula = new Gson().toJson(clasePersona);
            clasePersona.setIdPersona(0); // Si hay más registros SOLO traigase el primero que encuentre
            clasePersona.setRolPersona("");
            clasePersona.setNombrePersona("");
            clasePersona.setApellidoPersona("");
            clasePersona.setUsuarioPersona("");
            clasePersona.setClavePersona("");
            clasePersona.setTipoIdentificacion("");
            clasePersona.setNumeroIdentificacion("");
            clasePersona.setDireccionPersona("");
            clasePersona.setCiudadPersona("");
            clasePersona.setEmailPersona("");
            clasePersona.setTelefonoPersona("");
            clasePersona.setFechaLogging("");
    
            return respuestaPersonaPorCedula;
        } else { // False           
            return new Gson().toJson(clasePersona);
        }
    }

   @GetMapping("/producto")
    public String consultarProductoPorId(@RequestParam(value = "idProducto", defaultValue = "1") int idProducto) throws ClassNotFoundException, SQLException {
        claseProducto.setIdProducto(idProducto);
        if (claseProducto.consultarProducto()) { // True  // hace falta el metodo consultarProducto  que devuelve true en la clase Producto
      return new Gson().toJson(claseProducto);
        } else {
            return new Gson().toJson(claseProducto);
        }
    }

     @GetMapping("/facturas")
    public String consultarFacturasPorIdPersona(@RequestParam(value = "idPersona", defaultValue = "1") int idPersona) throws ClassNotFoundException, SQLException {
        List<Factura> listaFacturas = claseFactura.consultarTodoFactura(idPersona);
        if(listaFacturas.size() > 0){
            return new Gson().toJson(listaFacturas);
        } else {
             return new Gson().toJson(listaFacturas);
        }
 
 // GET - POST - PUT - DELETE
    @PostMapping(path = "/persona", // le enviamos datos al servidor
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición
  

    // {nombre = Estefania, edad = 25}
    public String actualizarPersona(@RequestBody String persona) throws ClassNotFoundException, SQLException{
        Persona f = new Gson().fromJson(persona, Persona.class); // recibimos el json y lo devolvemos un objeto persona
        clasePersona.setIdPersona(f.getIdPersona()); // obtengo el IdPersona de clasePersona y se lo ingreso e
        clasePersona.setRolPersona(f.emailPersona());
        clasePersona.setNombrePersona(f.getNombrePersona());
        clasePersona.setApellidoPersona(f.getApellidoPersona());
        clasePersona.actualizar();
        return new Gson().toJson(clasePersona); // cliente le envia json al servidor. fpr de comunicarse
  
     
    @DeleteMapping("/eliminarpersona/{id}")
    public String borrarProducto(@PathVariable("id") int id) throws SQLException, ClassNotFoundException{
        clasePersona.setIdPersona(id);
        clasePersona.borrar();
        return "Los datos del id indicado han sido eliminados";
    }

    
} // fin de la clase main
